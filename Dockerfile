FROM wordpress:latest
VOLUME /var/www/html/wp-content/uploads
EXPOSE 80
EXPOSE 8080
CMD ["apache2-foreground"]